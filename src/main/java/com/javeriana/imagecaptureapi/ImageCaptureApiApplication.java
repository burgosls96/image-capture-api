package com.javeriana.imagecaptureapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageCaptureApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageCaptureApiApplication.class, args);
	}

}
