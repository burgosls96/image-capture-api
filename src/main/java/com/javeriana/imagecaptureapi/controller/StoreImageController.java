package com.javeriana.imagecaptureapi.controller;

import com.javeriana.imagecaptureapi.enums.EmotionsEnum;
import com.javeriana.imagecaptureapi.interfaces.StoreImageInterface;
import com.javeriana.imagecaptureapi.model.RequestObject;
import com.javeriana.imagecaptureapi.model.ResponseObject;
import com.javeriana.imagecaptureapi.services.FileWriterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
public class StoreImageController implements StoreImageInterface {

    private static final Logger LOG = Logger.getLogger(StoreImageController.class.getName());

    private Base64.Decoder decoder;
    private DateTimeFormatter formatter;
    private static final int SCALED_SIZE = 299;
    private final FileWriterService fileWriterService;

    @Autowired
    public StoreImageController(FileWriterService fileWriterService){
        this.fileWriterService = fileWriterService;
    }

    @PostConstruct
    public void initDecoder(){
        this.decoder = Base64.getDecoder();
        formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH,mm,ss");
    }

    @Override
    public ResponseEntity<ResponseObject<String, String>> storeImage(@RequestBody RequestObject requestObject) {
        LOG.info("****** Entered storeImage method, in StoreImageController class ******");

        //Get project path
        String basePath = Paths.get("").toAbsolutePath().toString();

        EmotionsEnum emotionsEnum = requestObject.getEmotionDescription();
        String base64Image = requestObject.getBase64Image();
        LocalDateTime localDateTime = LocalDateTime.now();

        String imageCategory = emotionsEnum.getEmotion();
        String imageName = imageCategory + " " + localDateTime.format(formatter) + ".jpeg";

        byte[] decodedImage = this.decoder.decode(base64Image);

        String fileName = basePath + "/images/" + imageName;

        try{
            //Load image
            BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedImage));
            BufferedImage outputImage = new BufferedImage(SCALED_SIZE, SCALED_SIZE, bufferedImage.getType());
            Graphics2D graphics2D = outputImage.createGraphics();
            graphics2D.drawImage(bufferedImage, 0, 0, SCALED_SIZE, SCALED_SIZE, null);
            graphics2D.dispose();

            LOG.info("Image resized successfully.");

            //First, write image to folder
            ImageIO.write(outputImage, "jpeg", new File(fileName));

            //Then, add image info to CSV file
            this.fileWriterService.appendRegistryToFile(imageName, imageCategory);

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        }
        catch (IOException ex){
            LOG.severe("An error occurred processing the image file: " + ex.getMessage());
            ResponseObject<String, String> errorResponse = new ResponseObject<>();
            errorResponse.setNotifications(ex.getMessage());
            return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<ResponseObject<List<String>, String>> getEmotions() {
        LOG.info("****** Entered getEmotions method, in StoreImageController class ******");
        ResponseObject<List<String>, String> responseObject = new ResponseObject<>();
        responseObject.setData(Arrays.stream(EmotionsEnum.values()).map(EmotionsEnum::getEmotion).collect(Collectors.toList()));
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
