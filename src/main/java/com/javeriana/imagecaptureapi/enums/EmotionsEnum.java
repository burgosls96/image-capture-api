package com.javeriana.imagecaptureapi.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.List;

public enum EmotionsEnum {

    HAPPY("feliz"),
    SAD("triste"),
    CONFUSED("confundido"),
    BORED("aburrido"),
    FRUSTRATED("frustrado"),
    STRESSED("estresado"),
    ANXIOUS("ansioso");

    private String emotion;

    EmotionsEnum(String emotion){
        this.emotion = emotion;
    }

    @JsonValue
    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }
}
