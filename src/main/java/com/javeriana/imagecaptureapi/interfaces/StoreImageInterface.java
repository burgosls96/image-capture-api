package com.javeriana.imagecaptureapi.interfaces;

import com.javeriana.imagecaptureapi.model.RequestObject;
import com.javeriana.imagecaptureapi.model.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface StoreImageInterface {

    @GetMapping(path = "api/v1/emotions", produces = "application/json")
    default ResponseEntity<ResponseObject<List<String>, String>> getEmotions(){
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @PostMapping(path = "/api/v1/images", consumes = "application/json", produces="application/json")
    default ResponseEntity<ResponseObject<String, String>> storeImage(@RequestBody RequestObject requestObject){
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
