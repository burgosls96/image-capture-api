package com.javeriana.imagecaptureapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.javeriana.imagecaptureapi.enums.EmotionsEnum;
import lombok.*;

@Generated
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestObject {

    @JsonProperty("emotion")
    private EmotionsEnum emotionDescription;

    @JsonProperty("image")
    private String base64Image;

}
