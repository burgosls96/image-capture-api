package com.javeriana.imagecaptureapi.model;

public class ResponseObject<T,G> {

    private T data;
    private G notifications;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public G getNotifications() {
        return notifications;
    }

    public void setNotifications(G notifications) {
        this.notifications = notifications;
    }
}
