package com.javeriana.imagecaptureapi.services;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Logger;

@Service
public class FileWriterService {

    private FileWriter fileWriter;
    private CSVPrinter csvPrinter;
    private static final Logger LOG = Logger.getLogger(FileWriterService.class.getName());
    private static final String csvPath = Paths.get("").toAbsolutePath() + "/metadata/image_data.csv";

    @PostConstruct
    public void initFileWriterService() throws IOException {
        File csvMetadata;

        csvMetadata = new File(csvPath);
        if(!csvMetadata.exists()){
            LOG.info("CSV file \"image_data.csv\" does not exist. Creating file");
            this.fileWriter = new FileWriter(csvPath, true);
            CSVFormat.DEFAULT
                .withHeader("FileName", "Category")
                .print(this.fileWriter);

            LOG.info("CSV file \"image_data.csv\" created successfully.");
        }
        else{
            this.fileWriter = new FileWriter(csvPath, true);
            LOG.info("CSV file \"image_data.csv\" already exists. Skipping file creation process.");
        }
        this.csvPrinter = new CSVPrinter(this.fileWriter, CSVFormat.DEFAULT);
    }

    public void appendRegistryToFile(String imageName, String category) throws IOException {
        LOG.info("Writing to CSV file data related to image " + imageName);
        this.csvPrinter.printRecord(imageName, category);
        this.fileWriter.flush();
        LOG.info("Data of image " + imageName + " wrote successfully");
    }

    @PreDestroy
    public void closeFileWriter() throws IOException {
        this.fileWriter.flush();
        this.fileWriter.close();
    }

}
